# encoding: utf-8
require 'logstash/outputs/base'
require 'logstash/namespace'
require 'logstash/json'
require 'net/http'
require 'uri'

#
# This plugin runs actions on an Op5 Monitor server by calling its API. The Op5 Monitor API is available since Monitor version XX.
# Actions are used in order to process check results, manage downtimes, etc.
#
# Examples:
#
# . Process a check result based on syslog severity
#
# [source,ruby]
#     filter {
#       if [syslog_severity] == "error" {
#         mutate {
#           replace => { "exit_status" => "2" }
#         }
#       }
#     }
#     output {
#       monitor {
#         host           => 'demo.opsdis.com'
#         user           => 'op5api'
#         password       => 'supersecret'
#         action         => 'process-check-result'
#         action_config  => {
#           status_code   => "%{exit_status}"
#           plugin_output => "%{message}"
#         }
#         monitor_host    => "%{hostname}"
#         monitor_service => "dummy"
#       }
#     }
#
class LogStash::Outputs::Monitor < LogStash::Outputs::Base

  concurrency :single

  config_name 'monitor'

  # The hostname(s) of your Op5 server. If the hosts list is an array, Logstash will send the action to the first
  # entry in the list. If it disconnects, the same request will be processed to the next host. An action is send to each
  # host in the list, until one is accepts it. If all hosts are unavailable, the action is discarded. Ports can be
  # specified on any hostname, which will override the global port config.
  #
  # For example:
  # [source,ruby]
  #     "127.0.0.1"
  #     ["127.0.0.1", "127.0.0.2"]
  #     ["127.0.0.1:5665", "127.0.0.2"]
  config :host, :validate => :array, :default => ["127.0.0.1"]

  # Global port configuration. Can be overriten on any hostname.
  config :port, :validate => :number, :default => 443

  # The Op5 API user. This user must exist on your Op5 server. Make sure
  # this user has sufficient permissions to run the actions you configure.
  config :user, :validate => :string, :required => true

  # Password of the Op5 API user
  config :password, :validate => :password, :required => true

  # Connecting to the Op5 API is only available through SSL encryption. Set this setting to `false` to disable SSL
  # verification.
  config :ssl_verify, :validate => :boolean, :default => true

  # All actions must target an `monitor_host` or an `monitor_service`.
  config :action, :validate => ['process_service_check_result'], :required => true

  # Each action has its own parameters. Values of settings inside of `action_config` may include existing fields.
  config :action_config, :validate => :hash, :required => true

  # The Monitor `Host` object. This field may include existing fields.
  #
  # [source,ruby]
  #     monitor {
  #       [...]
  #       monitor_host => "%{hostname}"
  #     }
  config :monitor_host, :validate => :string, :required => true

  # The Monitor `Service` object. This field may include existing fields.
  #
  # [source,ruby]
  #     monitor {
  #       [...]
  #       monitor_host => "%{hostname}"
  #       monitor_service => "%{program}"
  #     }
  config :monitor_service, :validate => :string, :required => true

  # A hash of attributes for the object. The values can be existing fields.
  # The default is set to "'vars.created_by' => 'logstash'"
  #
  # Example:
  #
  # [source,ruby]
  #  object_attrs => {
  #    'vars.os' => "%{operatingsystem}"
  #  }
  config :object_attrs, :validate => :hash, :default => {'vars.created_by' => 'logstash'}

  ACTION_CONFIG_FIELDS = {
    'process_service_check_result' => {
    'status_code' => { 'required' => true },
    'plugin_output' => { 'required' => true },
    'performance_data' => {},
    'check_command' => {},
    'check_source' => {}
    }
  }

  public

  def register
    validate_action_config
    @ssl_verify ? @ssl_verify_mode = OpenSSL::SSL::VERIFY_PEER : @ssl_verify_mode = OpenSSL::SSL::VERIFY_NONE
    @host_id = 0
  end # def register

  public

  def receive(event)

    @available_hosts = @host.count

    begin
      @httpclient ||= connect
      request_body = Hash.new
      monitor_host = event.sprintf(@monitor_host)
      monitor_service = event.sprintf(@monitor_service)

      # Depending on the action we take, set either a filter in the request body or set a host and/or service in the
      # url parameters.

      case @action
      when 'process_service_check_result'
        request_body = {host_name: monitor_host, service_description: monitor_service}

      when 'process_host_check_result'
        request_body = {host_name: monitor_host}
      end

      @action_config.each do |key, value|
        request_body[key] = event.sprintf(value)
      end

      request = Net::HTTP::Post.new(@uri.request_uri)
      request.initialize_http_header({'Accept' => 'application/json'})
      request.initialize_http_header({'Content-type' => 'application/json'})
      request.basic_auth(@user, @password.value)
      request.body = LogStash::Json.dump(request_body)

      response = @httpclient.request(request)

      if response.code == '200'
        @logger.debug("Action '#{@action}' succeeded")
      end

      raise StandardError if response.code != '200'
      response_body = LogStash::Json.load(response.body)

    rescue Timeout::Error => e
      @logger.warn( "Request failed",
      :host => @uri.host, :port => @uri.port,
      :path => request.path, :body => request.body,
      :error => e )
      # If a host is not reachable, try the same request with the next host in the list. Try each host host only once per
      # request.
      if not (@available_hosts -= 1).zero?
        @httpclient = connect
        @logger.info("Retrying request with '#{@uri.host}:#{@uri.port}'")
        retry
      end

    rescue StandardError => e
      @logger.warn( "Request failed",
      :host => @uri.host, :port => @uri.port,
      :path => request.path, :body => request.body,
      :response_code => response.code, :response_body => response.body,
      :error => e )

    end
  end # def event

  private

  def validate_action_config
    ACTION_CONFIG_FIELDS[@action].each do |field, settings|
      if settings['required'] && !@action_config.key?(field)
        @logger.error("Setting '#{field}' is required for action '#{@action}'")
      end
    end

    @action_config.each_key do |field|
      if not ACTION_CONFIG_FIELDS[@action].key?(field)
        @logger.warn("Unknown setting '#{field}' for action '#{action}'")
      end
    end
  end # def validate_action_config

  def connect
    @current_host, @current_port = @host[@host_id].split(':')
    @host_id = @host_id + 1 >= @host.length ? 0 : @host_id + 1

    if not @current_port
      @current_port = @port
    end

    @uri = URI.parse("https://#{@current_host}:#{@current_port}/api/command/#{@action.upcase}")

    http = Net::HTTP.new(@uri.host, @uri.port)
    #    @logger.debug("CONFIG request with '#{@uri.host}:#{@uri.port}'")
    http.use_ssl = true
    http.verify_mode = @ssl_verify_mode
    http.open_timeout = 2
    http.read_timeout = 5
    http
  end # def http_connect

end # class LogStash::Outputs::Monitor
