# encoding: utf-8
require 'logstash/devutils/rspec/spec_helper'
require 'logstash/outputs/monitor'
require 'logstash/event'
require 'webmock/rspec'

describe LogStash::Outputs::Monitor do
  let(:host) { ['monitor.com:443'] }
  let(:user) { 'api' }
  let(:password) { 'secret' }
  let(:action) { 'process_service_check_result' }
  let(:action_config) { {'status_code' => '2', 'plugin_output' => 'Error | val1=1'} }
  let(:monitor_host) { 'srv-1' }
  let(:monitor_service) { 'dummy' }
  let(:options) {
    {
      'host' => host,
      'user' => user,
      'password' => password,
      'action' => action,
      'action_config' => action_config,
      'monitor_host' => monitor_host,
      'monitor_service' => monitor_service
    }
  }
  let(:event) { LogStash::Event.new({ 'message' => 'This is a dummy message.' }) }
  let(:request) { "https://#{host[0]}//api/command/PROCESS_SERVICE_CHECK_RESULT" }
  let(:request_body) { "{\"host_name\":\"#{monitor_host}\",\"service_description\":\"#{monitor_service}\",\"status_code\":\"2\",\"plugin_output\":\"Error | val1=1\"}" }
  let(:response_body) { "{\"result\":\"Successfully submitted PROCESS_SERVICE_CHECK_RESULT\"}" }
  let(:output) { LogStash::Outputs::Monitor.new(options) }
  let(:logger) { output.logger }

  before do
    output.register
  end

  after do
    WebMock.reset!
  end

  context 'with working configuration' do
    it 'should register without errors' do
      plugin = LogStash::Plugin.lookup('output', 'monitor').new(options)
      expect { plugin.register }.to_not raise_error
    end

    it 'should send the event to monitor' do
      
      stub_request(:post, "https://monitor.com/api/command/PROCESS_SERVICE_CHECK_RESULT").
              with(:body => "{\"host_name\":\"srv-1\",\"service_description\":\"dummy\",\"status_code\":\"2\",\"plugin_output\":\"Error | val1=1\"}",
                   :headers => {'Authorization'=>'Basic YXBpOnNlY3JldA==', 'Content-Type'=>'application/json'}).
              to_return(:status => 200, :body => "", :headers => {})
                      
      expect(logger).to receive(:debug).with("Action 'process_service_check_result' succeeded")
      output.receive(event)
    end
  end

end

